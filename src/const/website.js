/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

export default {
  title: "SoWork",
  logo: "",
  author: "SoWork",
  whiteList: ["/login", "/404", "/401", "/lock"],
  info: {
    title: "Sowork 综合业务管理系统",
    list: [
      'SoWork 综合业务管理系统',
      '当前版本：v1.0.0'
    ]
  },
  wel: {
    title: 'SoWork 综合业务管理系统',
    list: [
      'SoWork 综合业务管理系统',
      '最大程度上帮助企业节省时间成本和费用开支。',
      '当前版本：v1.0.0'
    ]
  }
}
