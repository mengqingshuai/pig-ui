import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/init/settings/index',
    method: 'get',
    params: query
  })
}

export function putFiscalYear(obj) {
  return request({
    url: '/init/settings/fiscalYear',
    method: 'put',
    data: obj
  })
}

export function putInitPwd(obj) {
  return request({
    url: '/init/settins/pwd',
    method: 'put',
    data: obj
  })
}
