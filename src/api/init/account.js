import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/init/account/accountPage',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/init/account/',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/init/account/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/init/account/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/init/account',
    method: 'put',
    data: obj
  })
}
